﻿using Android.App;
using Android.OS;

namespace costCalculator.Droid.Views
{
	[Activity(Label = "Reading for View")]
	public class ReadingView : BaseView
	{
		protected override int LayoutResource => Resource.Layout.ReadingScreen;

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			SupportActionBar.SetDisplayHomeAsUpEnabled(false);
		}
	}
}

