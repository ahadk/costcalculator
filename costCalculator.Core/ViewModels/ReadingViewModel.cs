﻿using MvvmCross.Core.ViewModels;

namespace costCalculator.Core.ViewModels
{
	public class ReadingViewModel
		: MvxViewModel
	{
		//private int _cost = 0;
		//public int Cost
		//{
		//	get { return _cost; }
		//	set { SetProperty(ref _cost, MeterReading/25); }
		//}

		private int _cost = 0;
		public int Cost
		{
			get
			{
				return this._cost;
			}
			set
			{
				_cost = value;
				RaisePropertyChanged(() => Cost);
			}
		}

		private int _meterReading = 0;
		public int MeterReading
		{
			get
			{
				return this._meterReading;
			}
			set
			{
				_meterReading = value;
				Cost = _meterReading / 25;
				RaisePropertyChanged(() => MeterReading);
			}
		}

		private int _serviceNumber = 0;
		public int ServiceNumber
		{
			get
			{
				return this._serviceNumber;
			}
			set
			{
				_serviceNumber = value;

				RaisePropertyChanged(() => ServiceNumber);
			}
		}

	}
}

