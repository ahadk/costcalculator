﻿using System;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using costCalculator.Core.ViewModels;

namespace costCalculator.iOS.Views
{
	[MvxFromStoryboard]
	public partial class ReadingView : MvxViewController
	{
		public ReadingView(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			var set = this.CreateBindingSet<ReadingView, ReadingViewModel>();
			set.Bind(meterreading).To(vm => vm.MeterReading);
			set.Bind(servicenum).To(vm => vm.ServiceNumber);
			set.Bind(cost).To(vm => vm.Cost);
			set.Apply();
		}
	}
}
